﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PAI_Lab1
{
    public partial class Glowna : System.Web.UI.Page
    {
        private TextBox[,] MKMacierzA;
        private TextBox[,] MKMacierzB;
        private TextBox[,] MKMacierzC;

        protected void Page_Load(object sender, EventArgs e)
        {
        }    

        private void loadMatrices()
        {
            MKMacierzA = new TextBox[,] { { tb111, tb112, tb113 }, { tb121, tb122, tb123 }, { tb131, tb132, tb133 } };
            MKMacierzB = new TextBox[,] { { tb211, tb212, tb213 }, { tb221, tb222, tb223 }, { tb231, tb232, tb233 } };
            MKMacierzC = new TextBox[,] { { tb311, tb312, tb313 }, { tb321, tb322, tb323 }, { tb331, tb332, tb333 } };

        }


        protected void generateA(object sender, EventArgs e)
        {
            Random rng = new Random();
            loadMatrices();
            int min = int.Parse(tbminA.Text);
            int max = int.Parse(tbmaxA.Text);

            foreach(TextBox t in MKMacierzA)
            {
                t.Text = "" + rng.Next(min, max);
            }
        }

        protected void generateB(object sender, EventArgs e)
        {
            Random rng = new Random();
            loadMatrices();
            int min = int.Parse(tbminB.Text);
            int max = int.Parse(tbmaxB.Text);

            foreach (TextBox t in MKMacierzB)
            {
                t.Text = "" + rng.Next(min, max);
            }
        }


        protected void close(object sender, EventArgs e)
        {
            Response.Write("<script language=javascript>window.close();</script>");
        }


        protected void sumuj(object sender, EventArgs e)
        {
            mkdzialanie.Text = "+";
            loadMatrices();

            int[,] x = new int[3,3];
            int[,] y = new int[3,3];
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    x[i,j] = int.Parse(MKMacierzA[i, j].Text);
                    y[i,j] = int.Parse(MKMacierzB[i, j].Text);
                    MKMacierzC[i, j].Text = "" + (x[i, j] + y[i, j]);
                }
        }

        protected void odejmuj(object sender, EventArgs e)
        {
            mkdzialanie.Text = "-";
            loadMatrices();

            int[,] x = new int[3, 3];
            int[,] y = new int[3, 3];
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    x[i, j] = int.Parse(MKMacierzA[i, j].Text);
                    y[i, j] = int.Parse(MKMacierzB[i, j].Text);
                    MKMacierzC[i, j].Text = "" + (x[i, j] - y[i, j]);
                }
        }

        protected void pomnoz(object sender, EventArgs e)
        {
            mkdzialanie.Text = "x";
            loadMatrices();

            int[,] x = new int[3, 3];
            int[,] y = new int[3, 3];
            int[,] z = new int[3, 3];
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    x[i, j] = int.Parse(MKMacierzA[i, j].Text);
                    y[i, j] = int.Parse(MKMacierzB[i, j].Text);
                }
            
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int suma = 0;
                    for (int k = 0; k < 3; k++)
                    {
                        suma = suma + x[i, k] * y[k, j];
                    }
                    MKMacierzC[i, j].Text = "" + suma;
                }
            }
        }

        protected void Walidacja(object sender, EventArgs e)
        {
            
            foreach (BaseValidator mkval in Page.Validators)
            {
                Control t = FindControl(mkval.ControlToValidate);
                TextBox tb = (TextBox) t;
                if(tb.ID == ((Control)sender).ID)
                {
                    tb.CausesValidation = true;
                    mkval.Validate();
                    if (!mkval.IsValid)
                    {
                        tb.Text = "0";
                        tb.CssClass = "textbox invalid";

                        r1.Enabled = false;
                        r2.Enabled = false;
                        r3.Enabled = false;

                        mkval.IsValid = false;
                    }
                    else
                    {   
                        tb.CssClass = "textbox";
                        r1.Enabled = true;
                        r2.Enabled = true;
                        r3.Enabled = true;
                    }
                    tb.CausesValidation = false;
                }
            }
        }

        protected void WalidacjaRange(object sender, EventArgs e)
        {
            ValidatorCollection vals = Page.Validators;
            foreach (BaseValidator mkval in vals)
            {
                Control t = FindControl(mkval.ControlToValidate);
                TextBox tb = (TextBox)t;
                if (tb.ID == ((Control)sender).ID)
                {
                    tb.CausesValidation = true;
                    mkval.Validate();
                    if (!mkval.IsValid)
                    {
                        tb.CssClass = "tbrange rangeinvalid";
                        mkval.IsValid = false;
                        r1.Enabled = false;
                        r2.Enabled = false;
                        r3.Enabled = false;
                    }
                    else
                    {
                        tb.CssClass = "tbrange";
                        r1.Enabled = true;
                        r2.Enabled = true;
                        r3.Enabled = true;
                    }
                    tb.CausesValidation = false;
                }
            }
        }
    }
}