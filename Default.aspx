﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PAI_Lab1.Glowna" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="Properties/styl.css" rel="stylesheet" type="text/css" />
    <title>Strona Główna</title>
</head>

<body>
    <form runat="server">
            
        <div class="container">
            
           <div class="macierze">
                <div class="macierz">
                <asp:Label runat="server" Text="Macierz A" />
                <table>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb111"   OnTextChanged="Walidacja">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb112"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb113"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb121"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb122"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb123"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb131"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb132"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb133"  OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                    </tr>
                </table>
                    <asp:RangeValidator  runat="server" ID="vtb111" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb111"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb112" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb112"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb113" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb113"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb121" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb121"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb122" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb122"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb123" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb123"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb131" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb131"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb132" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb132"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb133" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb133"></asp:RangeValidator>

            </div>

            <asp:Label runat="server" ID="mkdzialanie" Text="+" CssClass="dzialanie"/>

            <div class="macierz">
                <asp:Label runat="server" Text="Macierz B" />
                <table>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb211" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb212" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb213" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb221" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb222" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb223" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb231" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb232" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb233" OnTextChanged="Walidacja" AutoPostBack="true">0</asp:TextBox>
                        </td>
                    </tr>
                </table>
                    <asp:RangeValidator  runat="server" ID="vtb211" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb211" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb212" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb212" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb213" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb213" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb221" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb221" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb222" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb222" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb223" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb223" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb231" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb231" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb232" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb232" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RangeValidator  runat="server" ID="vtb233" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Błędne dane!<br/>" ControlToValidate="tb233" SetFocusOnError="true"></asp:RangeValidator>
            </div>

            <asp:Label runat="server" ID="rownanie" Text="=" CssClass="dzialanie"/>

             <div class="macierz">
                 <asp:Label runat="server" Text="Wynik" />
                <table>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb311" Enabled="false">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb312" Enabled="false">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb313" Enabled="false">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb321" Enabled="false">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb322" Enabled="false">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb323" Enabled="false">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb331" Enabled="false">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb332" Enabled="false">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox CssClass="textbox" runat="server" ID="tb333" Enabled="false">0</asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
           </div>

            <div class="allcontrols">
                <div class="controls radio">
                     <asp:RadioButton ID="r1" CssClass="radio" runat="server" groupName="radio1" OnCheckedChanged="sumuj" AutoPostBack="true" text=" Sumowanie" Checked="true"/><br />
                     <asp:RadioButton ID="r2" CssClass="radio" runat="server" groupName="radio1" OnCheckedChanged="odejmuj" AutoPostBack="true" text=" Odejmowanie"/><br />
                     <asp:RadioButton ID="r3" CssClass="radio" runat="server" groupName="radio1" OnCheckedChanged="pomnoz" AutoPostBack="true" text=" Mnożenie"/><br />
                </div>
                <div class="controls">
                    <table>
                        <tr>
                            <td>&nbsp</td>
                            <td>Min</td>
                            <td>Max</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button CssClass="button" runat="server" ID="mkgenerateA" OnClick="generateA" Text="Generuj macierz A" />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbminA" CssClass="tbrange" OnTextChanged="WalidacjaRange" AutoPostBack="true" Text="-99"/>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbmaxA" CssClass="tbrange" OnTextChanged="WalidacjaRange" AutoPostBack="true" Text="99"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button CssClass="button" runat="server" ID="mkgenerateB" OnClick="generateB" Text="Generuj macierz B" />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbminB" CssClass="tbrange" OnTextChanged="WalidacjaRange" AutoPostBack="true" Text="-99"/>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbmaxB" CssClass="tbrange" OnTextChanged="WalidacjaRange" AutoPostBack="true" Text="99"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button CssClass="button" runat="server" ID="CloseButton" OnClick="close" Text="Zakończ aplikację" />
                            </td>
                            <td>
                                <asp:RangeValidator runat="server"  ID="minAval" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Dolna wartość musi być większa od -99!" ControlToValidate="tbminA">Min A < -99 </asp:RangeValidator>
                                <asp:RangeValidator runat="server" ID="maxAval" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Górna wartość musi być mniejsza od 99!" ControlToValidate="tbmaxA">Max A > 99 </asp:RangeValidator>
                                <asp:CompareValidator Type="Integer" runat="server" V ID="minmaxA" ControlToValidate="tbmaxA" ControlToCompare="tbminA" Operator="GreaterThanEqual" Display="Dynamic" ErrorMessage="Minimum musi być mniejsze niż maksimum!" >Min A > Max A </asp:CompareValidator>
                                <asp:CompareValidator Type="Integer" runat="server" ID="maxminA" ControlToValidate="tbminA" ControlToCompare="tbmaxA" Operator="LessThanEqual" Display="Dynamic" ErrorMessage="Maksimum musi być większe niż minimum!" > Min A > Max A</asp:CompareValidator>

                                <asp:RangeValidator runat="server" ID="minBval" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Dolna wartość musi być większa od -99!" ControlToValidate="tbminB">Min B < -99</asp:RangeValidator>
                                <asp:RangeValidator runat="server" ID="maxBval" MinimumValue="-99" MaximumValue="99" Type="Integer" Display="Dynamic" ErrorMessage="Górna wartość musi być mniejsza od 99!" ControlToValidate="tbmaxB">Max B > 99</asp:RangeValidator>
                                <asp:CompareValidator Type="Integer" runat="server" ID="minmaxB" ControlToValidate="tbmaxB" ControlToCompare="tbminB" Operator="GreaterThanEqual" Display="Dynamic" ErrorMessage="Minimum musi być mniejsze niż maksimum!" >Min B > Max B </asp:CompareValidator>
                                <asp:CompareValidator Type="Integer" runat="server" ID="maxminB" ControlToValidate="tbminB" ControlToCompare="tbmaxB" Operator="LessThanEqual" Display="Dynamic" ErrorMessage="Maksimum musi być większe niż minimum!" > Min B > Max B</asp:CompareValidator>

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="validation">
                <asp:ValidationSummary ID="summary" runat="server" ForeColor="Red"/>  
            </div>
        </div>
        
    </form>
</body>

</html>